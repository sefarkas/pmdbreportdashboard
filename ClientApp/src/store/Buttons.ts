﻿import { Action, Reducer } from 'redux';
import { AppThunkAction } from './';

// -----------------
// STATE - This defines the type of data maintained in the Redux store.


export interface pmdbButton {
    "fldCtlCaption": string;
    "fldCtlLinkTo": string,
    "fldCtlName": string;
    "fldCtlColor": string;
    "fldCtlShowTip": boolean;
    "fldCtlCoor": string;
    "id": number;
}

export interface colA {
    "pmdbButton": pmdbButton[];
}
export interface colB {
    "pmdbButton": pmdbButton[];
}
export interface colC {
    "pmdbButton": pmdbButton[];
}
export interface colD {
    "pmdbButton": pmdbButton[];
}

export interface cells {
    "colA": colA[];
    "colB": colB[];
    "colC": colC[];
    "colD": colD[];
};


export interface ButtonsState {
    "buttonProperties"?: pmdbButton[];
    "colA": colA[];
    "colB": colB[];
    "colC": colC[];
    "colD": colD[];
    "isBusy": boolean;
}


export const REQUEST = 'REQUEST_BUTTON_PROPERTIES';
export const RECEIVE = 'RECEIVE_BUTTON_PROPERTIES';
export const INITIAL = 'INITIAL_BUTTON_PROPERTIES';

// -----------------
// ACTIONS - These are serializable (hence replayable) descriptions of state transitions.
// They do not themselves have any side-effects; they just describe something that is going to happen.


interface InitialButtonsAction {
    type: 'INITIAL_BUTTON_PROPERTIES';
    "colA": pmdbButton[];
    "colB": pmdbButton[];
    "colC": pmdbButton[];
    "colD": pmdbButton[];
}
// Declare a 'discriminated union' type. This guarantees that all references to 'type' properties contain one of the
// declared type strings (and not any other arbitrary string).
type KnownAction =  InitialButtonsAction;

// ----------------
// ACTION CREATORS - These are functions exposed to UI components that will trigger a state transition.
// They don't directly mutate state, but they can have external side-effects (such as loading data).

export const actionCreators = {
    initButtons: (): AppThunkAction<KnownAction> => (dispatch, getState) => {
        const appState = getState();
        if (appState && appState.buttonAttributes) {
            dispatch({
                type: INITIAL,
                "colA": kColA,
                "colB": kColB,
                "colC": kColC,
                "colD": kColD
            });
        }
    },
};

// ----------------
// REDUCER - For a given state and action, returns the new state. To support time travel, this must not mutate the old state.

export const INITIAL_STATE: any =
    [{
        "cells": [
            {
                "colA": [

                    {
                        "fldCtlName": "Preview NYSI Light Duty",
                        "fldCtlLinkTo": "http://projectserver.dsnyad.nycnet/bit/apps/NTI/_layouts/ReportServer/RSViewerPage.aspx?rv:RelativeReportUrl=/bit/apps/NTI/NTI%20Reports/PMdatabaseReportsSSAD/LightDutyReport.rdl ",
                        "fldCtlCaption": "first stream",
                        "fldCtlColor": "primary",
                        "fldCtlShowTip": "false", "fldCtlCoor": "A1", "id": 1
                    },
                    {
                        "fldCtlName": "Preview NYSI Expiration",
                        "fldCtlLinkTo": "http://projectserver.dsnyad.nycnet/bit/apps/NTI/_layouts/ReportServer/RSViewerPage.aspx?rv:RelativeReportUrl=/bit/apps/NTI/NTI%20Reports/PMdatabaseReportsSSAD/PreviewNYSIInspectionReport.rdl",
                        "fldCtlCaption": "second stream description",
                        "fldCtlColor": "primary",
                        "fldCtlShowTip": "false",
                        "fldCtlCoor": "A2",
                        "id": 2
                    },
                    {
                        "fldCtlName": "31 and 32 Distribution",
                        "fldCtlLinkTo": "http://projectserver.dsnyad.nycnet/bit/apps/NTI/_layouts/ReportServer/RSViewerPage.aspx?rv:RelativeReportUrl=/bit/apps/NTI/NTI%20Reports/PMdatabaseReportsSSAD/Preview3132Report.rdl",
                        "fldCtlCaption": "this one has a user ID",
                        "fldCtlColor": "primary",
                        "fldCtlShowTip": "false",
                        "fldCtlCoor": "A3",
                        "id": 3
                    }

                ],
                "colB": [

                    {
                        "fldCtlName": "Veh/Equip Summary",
                        "fldCtlLinkTo": "http://projectserver.dsnyad.nycnet/bit/apps/NTI/_layouts/ReportServer/RSViewerPage.aspx?rv:RelativeReportUrl=/bit/apps/NTI/NTI%20Reports/PMdatabaseReportsSSAD/PreviewVehEquipSummary.rdl",
                        "fldCtlCaption": "first stream",
                        "fldCtlColor": "secondary",
                        "fldCtlShowTip": "false",
                        "fldCtlCoor": "B1",
                        "id": 1
                    },
                    {
                        "fldCtlName": "Series Sumary",
                        "fldCtlLinkTo": "http://projectserver.dsnyad.nycnet/bit/apps/NTI/_layouts/ReportServer/RSViewerPage.aspx?rv:RelativeReportUrl=/bit/apps/NTI/NTI%20Reports/PMdatabaseReportsSSAD/FD05_B02_ROUTING.rdl",
                        "fldCtlCaption": "second stream description",
                        "fldCtlColor": "secondary",
                        "fldCtlShowTip": "false",
                        "fldCtlCoor": "B2",
                        "id": 2
                    },
                    {
                        "fldCtlName": "Series Milage",
                        "fldCtlLinkTo": "http://projectserver.dsnyad.nycnet/bit/apps/NTI/_layouts/ReportServer/RSViewerPage.aspx?rv:RelativeReportUrl=/bit/apps/NTI/NTI%20Reports/PMdatabaseReportsSSAD/PreviewSeriesMileage.rdl",
                        "fldCtlCaption": "this one has a user ID",
                        "fldCtlColor": "secondary",
                        "fldCtlShowTip": "false",
                        "fldCtlCoor": "B3",
                        "id": 3
                    },
                    {
                        "fldCtlName": "LDuty/PC Age",
                        "fldCtlLinkTo": "http://projectserver.dsnyad.nycnet/bit/apps/NTI/_layouts/ReportServer/RSViewerPage.aspx?rv:RelativeReportUrl=/bit/apps/NTI/NTI%20Reports/PMdatabaseReportsSSAD/PMChartLiteDutyPassCarAge.rdl",
                        "fldCtlCaption": "jump back to list after submit with PATCH",
                        "fldCtlColor": "secondary",
                        "fldCtlShowTip": "false",
                        "fldCtlCoor": "B4",
                        "id": 4
                    },
                    {
                        "fldCtlName": "LDuty/PC Age Boro",
                        "fldCtlLinkTo": "http://projectserver.dsnyad.nycnet/bit/apps/NTI/_layouts/ReportServer/RSViewerPage.aspx?rv:RelativeReportUrl=/bit/apps/NTI/NTI%20Reports/PMdatabaseReportsSSAD/PMChartLiteSeriesSnapShot.rdl",
                        "fldCtlCaption": "jump back to list after submit with PATCH",
                        "fldCtlColor": "secondary",
                        "fldCtlShowTip": "false",
                        "fldCtlCoor": "B5",
                        "id": 5
                    },
                    {
                        "fldCtlName": "EqType Age",
                        "fldCtlLinkTo": "http://projectserver.dsnyad.nycnet/bit/apps/NTI/_layouts/ReportServer/RSViewerPage.aspx?rv:RelativeReportUrl=/bit/apps/NTI/NTI%20Reports/PMdatabaseReportsSSAD/EquipmentTypeAge.rdl",
                        "fldCtlCaption": "jump back to list after submit with PATCH",
                        "fldCtlColor": "secondary",
                        "fldCtlShowTip": "false",
                        "fldCtlCoor": "B6",
                        "id": 6
                    },
                    {
                        "fldCtlName": "Boro Interval Summary",
                        "fldCtlLinkTo": "http://projectserver.dsnyad.nycnet/bit/apps/NTI/_layouts/ReportServer/RSViewerPage.aspx?rv:RelativeReportUrl=/bit/apps/NTI/NTI%20Reports/PMdatabaseReportsSSAD/PMMilesHrsIntervalSummary.rdl",
                        "fldCtlCaption": "jump back to list after submit with PATCH",
                        "fldCtlColor": "secondary",
                        "fldCtlShowTip": "false",
                        "fldCtlCoor": "B7",
                        "id": 7
                    },
                    {
                        "fldCtlName": "300 Hour Projection",
                        "fldCtlLinkTo": "http://projectserver.dsnyad.nycnet/bit/apps/NTI/_layouts/ReportServer/RSViewerPage.aspx?rv:RelativeReportUrl=/bit/apps/NTI/NTI%20Reports/PMdatabaseReportsSSAD/300HourProjector.rdl",
                        "fldCtlCaption": "made with a common form with PUT",
                        "fldCtlColor": "secondary",
                        "fldCtlShowTip": "false",
                        "fldCtlCoor": "B8",
                        "id": 8
                    }

                ],
                "colC": [

                    {
                        "fldCtlName": "Manufacturer",
                        "fldCtlLinkTo": "http://projectserver.dsnyad.nycnet/bit/apps/NTI/_layouts/ReportServer/RSViewerPage.aspx?rv:RelativeReportUrl=/bit/apps/NTI/NTI%20Reports/PMdatabaseReportsSSAD/DOSFleetManufacturer.rdl",
                        "fldCtlCaption": "first stream",
                        "fldCtlColor": "success",
                        "fldCtlShowTip": "false",
                        "fldCtlCoor": "C1",
                        "id": 1
                    },
                    {
                        "fldCtlName": "Fleet Miles Hours Age",
                        "fldCtlLinkTo": "http://projectserver.dsnyad.nycnet/bit/apps/NTI/_layouts/ReportServer/RSViewerPage.aspx?rv:RelativeReportUrl=/bit/apps/NTI/NTI%20Reports/PMdatabaseReportsSSAD/SeriesMileageDetailedSupportDataGroupByEquip.rdl",
                        "fldCtlCaption": "second stream description",
                        "fldCtlColor": "success",
                        "fldCtlShowTip": "false",
                        "fldCtlCoor": "C2",
                        "id": 2
                    },
                    {
                        "fldCtlName": "Boro Fleet",
                        "fldCtlLinkTo": "http://projectserver.dsnyad.nycnet/bit/apps/NTI/_layouts/ReportServer/RSViewerPage.aspx?rv:RelativeReportUrl=/bit/apps/NTI/NTI%20Reports/PMdatabaseReportsSSAD/SeriesEquipmentCountReport.rdl",
                        "fldCtlCaption": "this one has a user ID",
                        "fldCtlColor": "success",
                        "fldCtlShowTip": "false",
                        "fldCtlCoor": "C3",
                        "id": 3
                    },
                    {
                        "fldCtlName": "Series/Equipment Count",
                        "fldCtlLinkTo": "http://projectserver.dsnyad.nycnet/bit/apps/NTI/_layouts/ReportServer/RSViewerPage.aspx?rv:RelativeReportUrl=/bit/apps/NTI/NTI%20Reports/PMdatabaseReportsSSAD/SeriesEquipmentCountReport.rdl",
                        "fldCtlCaption": "jump back to list after submit with PATCH",
                        "fldCtlColor": "success",
                        "fldCtlShowTip": "false",
                        "fldCtlCoor": "C4",
                        "id": 4
                    },
                    {
                        "fldCtlName": "Snow Equipment",
                        "fldCtlLinkTo": "http://projectserver.dsnyad.nycnet/bit/apps/NTI/_layouts/ReportServer/RSViewerPage.aspx?rv:RelativeReportUrl=/bit/apps/NTI/NTI%20Reports/PMdatabaseReportsSSAD/SnowEquipReport.rdl",
                        "fldCtlCaption": "jump back to list after submit with PATCH",
                        "fldCtlColor": "success",
                        "fldCtlShowTip": "false",
                        "fldCtlCoor": "C5",
                        "id": 5
                    },
                    {
                        "fldCtlName": "Specific Vehicle Picklist",
                        "fldCtlLinkTo": "http://projectserver.dsnyad.nycnet/bit/apps/NTI/_layouts/ReportServer/RSViewerPage.aspx?rv:RelativeReportUrl=/bit/apps/NTI/NTI%20Reports/PMdatabaseReportsSSAD/PMChartSpecificVehPickList.rdl",
                        "fldCtlCaption": "made with a common form with PUT",
                        "fldCtlColor": "success",
                        "fldCtlShowTip": "false",
                        "fldCtlCoor": "C6",
                        "id": 6
                    },
                    {
                        "fldCtlName": "Active PM Type",
                        "fldCtlLinkTo": "http://projectserver.dsnyad.nycnet/bit/apps/NTI/_layouts/ReportServer/RSViewerPage.aspx?rv:RelativeReportUrl=/bit/apps/NTI/NTI%20Reports/PMdatabaseReportsSSAD/PreviewActivePMtypeReport.rdl",
                        "fldCtlCaption": "made with a common form with PUT",
                        "fldCtlColor": "success",
                        "fldCtlShowTip": "false",
                        "fldCtlCoor": "C7",
                        "id": 7
                    }

                ],
                "colD": [
                    {
                        "fldCtlName": "Mileage Swap",
                        "fldCtlLinkTo": "http://projectserver.dsnyad.nycnet/bit/apps/NTI/_layouts/ReportServer/RSViewerPage.aspx?rv:RelativeReportUrl=/bit/apps/NTI/NTI%20Reports/PMdatabaseReportsSSAD/MileageSwap.rdl",
                        "fldCtlCaption": "first stream",
                        "fldCtlColor": "info",
                        "fldCtlShowTip": "false",
                        "fldCtlCoor": "D1",
                        "id": 1
                    },
                    {
                        "fldCtlName": "Series Mileage Age",
                        "fldCtlLinkTo": "http://projectserver.dsnyad.nycnet/bit/apps/NTI/_layouts/ReportServer/RSViewerPage.aspx?rv:RelativeReportUrl=/bit/apps/NTI/NTI%20Reports/PMdatabaseReportsSSAD/SeriesMileageDetailedSupportDataGroupByEquip.rdl",
                        "fldCtlCaption": "second stream description",
                        "fldCtlColor": "info",
                        "fldCtlShowTip": "false",
                        "fldCtlCoor": "D2",
                        "id": 2
                    },
                    {
                        "fldCtlName": "Blank Chart Locations",
                        "fldCtlLinkTo": "http://projectserver.dsnyad.nycnet/bit/apps/NTI/_layouts/ReportServer/RSViewerPage.aspx?rv:RelativeReportUrl=/bit/apps/NTI/NTI%20Reports/PMdatabaseReportsSSAD/VehiclePMLocationInfo.rdl",
                        "fldCtlCaption": "this one has a user ID",
                        "fldCtlColor": "info",
                        "fldCtlShowTip": "false",
                        "fldCtlCoor": "D3",
                        "id": 3
                    },
                    {
                        "fldCtlName": "DCAS Mile Hours",
                        "fldCtlLinkTo": "http://projectserver.dsnyad.nycnet/bit/apps/NTI/_layouts/ReportServer/RSViewerPage.aspx?rv:RelativeReportUrl=/bit/apps/NTI/NTI%20Reports/PMdatabaseReportsSSAD/PreviewDasMile.rdl",
                        "fldCtlCaption": "jump back to list after submit with PATCH",
                        "fldCtlColor": "info",
                        "fldCtlShowTip": "false",
                        "fldCtlCoor": "D4",
                        "id": 4
                    }
                ]
            }
        ]
    }];

const kColA = INITIAL_STATE[0].cells[0].colA;
const kColB = INITIAL_STATE[0].cells[0].colB;
const kColC = INITIAL_STATE[0].cells[0].colC;
const kColD = INITIAL_STATE[0].cells[0].colD;

const initialCellAttributes: cells = {
    "colA": kColA,
    "colB": kColB,
    "colC": kColC,
    "colD": kColD,
};

export const reducerCells: Reducer<cells> = (state = initialCellAttributes , incomingAction: Action): cells => {
        if (state === undefined) {
            return initialCellAttributes;
        }

        const action = incomingAction as KnownAction;

        switch (action.type) {
            case INITIAL:
                return state;
            default:
                return state;
        }

    };
