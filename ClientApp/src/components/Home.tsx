import * as React from 'react';
import './Home.css';

class Home extends React.Component {

    public render() {

        const noIE11 = this.ieMsg();
        
        return (
            <div>
                <h1>BME PM Data (PMdb)</h1>
                <h3><span> This is a guide to button arrangement on the <a href="./dashboard"><i>Dashboard</i></a></span>
                </h3>
                <h4>{noIE11}</h4>
                <div className="polaroid">
                </div>
            </div>
        );
    };

    private ieMsg() {
        if (this.detectIE() ) {
            return (
                <div className="noIE11">Use Edge or Chrome to use the Dashboard.</div>
            );
        }
        else {
            return null;
        }
    }
    private detectIE() {
        const ua = window.navigator.userAgent;


        const msie = ua.indexOf('MSIE ');
        if (msie > 0) {
            // IE 10 or older => return version number
            return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
        }

        const trident = ua.indexOf('Trident/');
        if (trident > 0) {
            // IE 11 => return version number
            const rv = ua.indexOf('rv:');
            return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
        }

        const edge = ua.indexOf('Edge/');
        if (edge > 0) {
            // Edge (IE 12+) => return version number
            // return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
            return false; // Edge has the Chromium engine and ought to behave like Chrome.
        }

        // other browser
        return false;
    }
}

// 
export default Home;
