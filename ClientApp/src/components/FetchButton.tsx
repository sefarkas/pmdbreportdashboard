﻿import * as React from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import { Button, Col, Container, Row, Tooltip } from 'reactstrap';
import { ApplicationState } from '../store';
import * as ButtonsStore from '../store/Buttons';

// At runtime, Redux will merge together...
type ButtonProps =
    ButtonsStore.ButtonsState // ... state we've requested from the Redux store
    & typeof ButtonsStore.actionCreators // ... plus action creators we've requested
    & RouteComponentProps<{ buttonIndex: string }> // ... plus incoming routing parameters
    ;


class FetchButtons extends React.PureComponent<ButtonProps & any, ButtonsStore.ButtonsState> {

    constructor(props: ButtonProps & any) {
        super(props);
        this.sayHello = this.sayHello.bind(this);
        // isBusy = true;
        // this.props.initButtons(); // fill the store with all button attributes
        const { colA, colB, colC, colD } = this.props;
       this.state = {
            'colA': colA,
            'colB': colB,
            'colC': colC,
           'colD': colD,
           'isBusy': (colA.length + colB.length + colC.length + colD.length > 0 ? false : true)
        };
    }

    // This method is called when the component is first added to the document
    public componentDidMount() {
        const { colA, colB, colC, colD } = this.props;
        if (colA.length + colB.length + colC.length + colD.length > 0) {
            this.setState(prevState => ({
                ...prevState,
                'isBusy': false,
            }));
        }
    }

    public componentWillUnmount() {
        this.setState(prevState => ({
            ...prevState,
            'isBusy': false,
        }));
    }

    // This method is called when the route parameters change
    public componentDidUpdate(prevProps: ButtonProps & any) {
        const { colA, colB, colC, colD } = this.props;
        if (
            (
                prevProps.colA !== colA ||
                prevProps.colB !== colB ||
                prevProps.colC !== colC ||
                prevProps.colD !== colD 
           )
            && (colA.length + colB.length + colC.length + colD.length > 0)) {
            this.setState(prevState => ({
                ...prevState,
                'colA': colA,
                'colB': colB,
                'colC': colC,
                'colD': colD,
                'isBusy': false
            }));
        }
    }

    public render() {
        return (
            <div>
                <React.Fragment>
                    {this.renderButtonOnTheFly()}
                </React.Fragment>
            </div>
        );
    }

    // private ensureDataFetched() {
    //    const buttonIndex = 'A2' // parseInt(this.props.match.params.buttonIndex, 10) || 0;
    //    this.props.requestButtons(buttonIndex);
    // }

    private updateButtonProperties(updateThisButtonWithPropsPassedIn: ButtonsStore.pmdbButton) {
        const { colA, colB, colC, colD } = this.props;
        const sCol: string = updateThisButtonWithPropsPassedIn.fldCtlCoor.substr(0, 1);
        const sID: string = updateThisButtonWithPropsPassedIn.fldCtlCoor.substr(1, 1);
        if (colA.length + colB.length + colC.length + colD.length > 0) {
            let iID: number = parseInt(sID, 10) // base 10 is a factor used in the conversion algorithm
            iID = iID - 1; // array index is zero for first item
            if (sCol === "A") {
                colA[iID] = updateThisButtonWithPropsPassedIn;
            }
            else if (sCol === "B") {
                colB[iID] = updateThisButtonWithPropsPassedIn;
            }
            else if (sCol === "C") {
                colC[iID] = updateThisButtonWithPropsPassedIn;
            }
            else if (sCol === "D") {
                colD[iID] = updateThisButtonWithPropsPassedIn;
            }
        };

        this.setState(prevState => ({
            ...prevState,
            'colA': colA,
            'colB': colB,
            'colC': colC,
            'colD': colD,
            'isBusy': true
        }));
    }
    private changeToolTip(aButtonCoor: string, newValueForShowTip: boolean) {
        const lclObjButton: ButtonsStore.pmdbButton & any = this.aButton(aButtonCoor);
        lclObjButton.fldCtlShowTip = newValueForShowTip;
        this.updateButtonProperties(lclObjButton);

    }

    private busy() {
        return (<div className="loader center">busy ...<i className="fa fa-cog fa-spin" /></div>)
    }

    private renderButtonOnTheFly() {
        console.log("rednderButtonOnTheFly available props");
        console.log(this.props);
        // const { buttonProperties } = this.props;

        const eOnClick = (e: any) => {
            this.sayHello(e);
        }

        const eOnMouseEnter = (e: any) => {
            return null; //  return isBusy ? null : this.changeToolTip(e.target.id, true);
        }

        const eOnMouseLeave = (e: any) => {
            return null; // return isBusy ? null : this.changeToolTip(e.target.id, false)
        }


        return (
            <div>
                <div>Arranged according to the guide.</div>

                <Container>
                    <Row>
                        <Col md="3" xs="12">
                            <Container>
                                <Row>
                                    <Col md="6">
                                        {this.GetItsLink("A1", eOnClick, eOnMouseEnter, eOnMouseLeave)}
                                    </Col>
                                </Row>

                                <Row>
                                    <Col md="6"  >
                                        {this.GetItsLink("A2", eOnClick, eOnMouseEnter, eOnMouseLeave) }
                                    </Col>
                                </Row>
                                <Row>
                                    <Col md="6">
                                        {this.GetItsLink("A3", eOnClick, eOnMouseEnter, eOnMouseLeave)}
                                    </Col>
                                </Row>

                            </Container>
                        </Col>
                        <Col md="3" xs="12">
                            <Container>
                                <Row>
                                    <Col md="6"  >
                                        {this.GetItsLink("B1", eOnClick, eOnMouseEnter, eOnMouseLeave)}
                            </Col>
                                </Row>

                                <Row>
                                    <Col md="6" >
                                        {this.GetItsLink("B2", eOnClick, eOnMouseEnter, eOnMouseLeave)}
                            </Col>
                                </Row>
                                <Row>
                                    <Col md="6"  >
                                        {this.GetItsLink("B3", eOnClick, eOnMouseEnter, eOnMouseLeave)}
                            </Col>
                                </Row>

                                <Row>
                                    <Col md="6" >
                                        {this.GetItsLink("B4", eOnClick, eOnMouseEnter, eOnMouseLeave)}
                            </Col>
                                </Row>
                                <Row>
                                    <Col md="6"  >
                                        {this.GetItsLink("B5", eOnClick, eOnMouseEnter, eOnMouseLeave)}
                            </Col>
                                </Row>

                                <Row>
                                    <Col md="6" >
                                        {this.GetItsLink("B6", eOnClick, eOnMouseEnter, eOnMouseLeave)}
                            </Col>
                                </Row>
                                <Row>
                                    <Col md="6"  >
                                        {this.GetItsLink("B7", eOnClick, eOnMouseEnter, eOnMouseLeave)}
                            </Col>
                                </Row>

                                <Row>
                                    <Col md="6" >
                                        {this.GetItsLink("B8", eOnClick, eOnMouseEnter, eOnMouseLeave)}
                            </Col>
                                </Row>
                            </Container>
                        </Col>
                        <Col md="3" xs="12">
                            <Container>
                                <Row>
                                    <Col md="6"  >
                                        {this.GetItsLink("C1", eOnClick, eOnMouseEnter, eOnMouseLeave)}
                            </Col>
                                </Row>

                                <Row>
                                    <Col md="6" >
                                        {this.GetItsLink("C2", eOnClick, eOnMouseEnter, eOnMouseLeave)}
                            </Col>
                                </Row>
                                <Row>
                                    <Col md="6"  >
                                        { this.GetItsLink("C3", eOnClick, eOnMouseEnter, eOnMouseLeave)}
                            </Col>
                                </Row>

                                <Row>
                                    <Col md="6" >
                                        {this.GetItsLink("C4", eOnClick, eOnMouseEnter, eOnMouseLeave)}
                            </Col>
                                </Row>
                                <Row>
                                    <Col md="6"  >
                                        {this.GetItsLink("C5", eOnClick, eOnMouseEnter, eOnMouseLeave)}
                            </Col>
                                </Row>

                                <Row>
                                    <Col md="6" >
                                        {this.GetItsLink("C6", eOnClick, eOnMouseEnter, eOnMouseLeave)}
                            </Col>
                                </Row>

                                <Row>
                                    <Col md="6" >
                                        {this.GetItsLink("C7", eOnClick, eOnMouseEnter, eOnMouseLeave)}
                                    </Col>
                                </Row>
                            </Container>
                        </Col>
                        <Col md="3" xs="12">
                            <Container>
                                <Row>
                                    <Col md="6">
                                        {this.GetItsLink("D1", eOnClick, eOnMouseEnter, eOnMouseLeave)}
                            </Col>
                                </Row>

                                <Row>
                                    <Col md="6"  >
                                        {this.GetItsLink("D2", eOnClick, eOnMouseEnter, eOnMouseLeave)}
                            </Col>
                                </Row>
                                <Row>
                                    <Col md="6">
                                        {this.GetItsLink("D4", eOnClick, eOnMouseEnter, eOnMouseLeave)}
                            </Col>
                                </Row>
                                <Row>
                                    <Col md="6">
                                        {this.GetItsLink("D5", eOnClick, eOnMouseEnter, eOnMouseLeave)}
                            </Col>
                                </Row>
                            </Container>
                        </Col>
                    </Row>
                </Container>
            </div>
            );
        };
    private sayHello(event: any) {
        const lclObjButton: ButtonsStore.pmdbButton & any = this.aButton(event.target.id);
        const sMsg = lclObjButton.fldCtlLinkTo;
        const sID = lclObjButton.fldCtlCoor;
        if (sMsg.substring(0, 4) === "http") {
            // alert(sMsg);
            window.location.replace(sMsg);
        } else {
            alert(sMsg + ' ' + sID);
        }
        
    }

    private aButton(buttonOfInterest: string) {
        const { colA, colB, colC, colD } = this.props;
        const sCol = buttonOfInterest.substr(0, 1);
        const sID = buttonOfInterest.substr(1, 1);
        if (colA.length + colB.length + colC.length + colD.length > 0) {
            let iID = parseInt(sID, 10) // base 10 is a factor used in the conversion algorithm
            iID = iID - 1; // array index is zero for first item
            let coor: ButtonsStore.pmdbButton = colA[iID]; // needs a default
            if (sCol === "B") {
                coor = colB[iID]
            }
            else if (sCol === "C") {
                coor = colC[iID]
            }
            else if (sCol === "D") {
                coor = colD[iID]
            }

            return coor;
        };
    };

    private GetItsLink(buttonOfInterest: string, eOnclick: any, eOnMouseEnter: any, eOnMouseLeave: any) {
        const { colA, colB, colC, colD } = this.props;
        const { isBusy } = this.state;
        if (colA.length + colB.length + colC.length + colD.length === 0 || isBusy ) {
            return (
                this.busy()
            )

        }
        if (colA.length + colB.length + colC.length + colD.length < 1) {
            return (
                this.busy()
            );
        };
        const coor: ButtonsStore.pmdbButton & any = this.aButton(buttonOfInterest)
        if (!coor) { return null };
        const ref = buttonOfInterest; // needed to fix order of drawing Button and Tooltip.
        // Tooltip is drawing first and cannot find ref of Button
        if (!!ref && colA.length + colB.length + colC.length + colD.length > 0) {
            const isOpenValue = coor.fldCtlShowTip;
            return (
                <div>
                    <Button id={buttonOfInterest} ref={ref}   varient={coor.fldCtlColor}
                        onMouseEnter={eOnMouseEnter} onMouseLeave={eOnMouseLeave}
                        onClick={eOnclick} value={coor.fldCtlName} active >{coor.fldCtlName}
                    </Button>
                    {isOpenValue
                        ? <Tooltip placement="right" target={ref} className="in" id="tooltip-right">
                            {coor.fldCtlCaption}
                            </Tooltip>
                        : ""
                    }
                </div>
            );
        }
        else {
            // console.log("store has no button properties yet")
            return (
                <div>
                    <Button id={buttonOfInterest} ref={ref} varient="primary"
                        onClick={eOnclick} value={buttonOfInterest} active >{buttonOfInterest}</Button>
                </div>
            );
        }

    }

}

export default connect(
    (state: ApplicationState) => state.buttonAttributes, // Selects which state properties are merged into the component's props
    ButtonsStore.actionCreators // Selects which action creators are merged into the component's props
)(FetchButtons as any);
