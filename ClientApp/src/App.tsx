import * as React from 'react';
import { Route } from 'react-router';
import FetchButtonsPretty from './components/FetchButtonPretty';
import Home from './components/Home';
import Layout from './components/Layout';
import './custom.css';


export default () => (
    <Layout>
        <Route exact path='/' component={Home} />
        <Route path='/dashboard' component={FetchButtonsPretty} />
    </Layout>
);
