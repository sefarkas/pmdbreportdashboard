using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace VS2019defaultReactRedux
{
    public class Program
    {
        public static void Main( string[] args )
        {
            // CreateHostBuilder( args ).Build().Run();
            CreateWebHostBuilder( args ).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder( string[] args ) =>
            Host.CreateDefaultBuilder( args )
                .ConfigureWebHostDefaults( webBuilder =>
                 {
                     webBuilder.UseStartup<Startup>();
                 }
        );

        public static IWebHostBuilder CreateWebHostBuilder( string[] args )
        {
            var env = Environment.GetEnvironmentVariable( "ASPNETCORE_ENVIRONMENT" );
            var builder = Microsoft.AspNetCore.WebHost.CreateDefaultBuilder( args );

            if (env == Microsoft.Extensions.Hosting.Environments.Staging || env == Microsoft.Extensions.Hosting.Environments.Production)
                builder.UseIIS();

            builder.UseStartup<Startup>();
            return builder;
        }
    }
}
